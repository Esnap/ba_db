FROM postgres:9.5

RUN apt-get update
RUN apt-get install -y less
RUN apt-get install -y vim                          
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#TODO: implement script   
ADD scripts/db-init.sh /docker-entrypoint-initdb.d/

ENV POSTGRES_USER=docker
ENV POSTGRES_PASSWORD=docker
ENV POSTGRES_DB docker
ENV POSTGRES_MAX_CONNECTIONS=200
ENV POSTGRES_SHARED_BUFFERS="256MB"

RUN chmod 755 -R /var/lib/postgresql
#CMD postgres -c max_connections=$POSTGRES_MAX_CONNECTIONS -c shared_buffers=$POSTGRES_SHARED_BUFFERS

USER postgres

